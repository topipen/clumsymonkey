﻿using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public delegate void GameDelegate();
	public static event GameDelegate OnGameStarted;
	public static event GameDelegate OnGameOverConfirmed;

	public static GameManager Instance;

	public GameObject startPage;
	public GameObject gameOverPage;
	public GameObject countdownPage;
	public GameObject settingsPage;
	public Text scoreText;

	// Settings
	public AudioSource backgroundMusic;
	public GameObject musicToggle;
	public GameObject soundToggle;
	public Sprite noMusicImage;
	public Sprite musicImage;
	public GameObject easyButton;
	public GameObject mediumButton;
	public GameObject hardButton;
	public Sprite checkboxSelected;
	public Sprite checkboxUnselected;

	enum PageState {
		None,
		Start,
		Countdown,
		GameOver,
		SettingsOpen
	}

	int score = 0;
	bool gameOver = true;

	public bool GameOver { get { return gameOver; } }

	void Awake() {
		if (Instance != null) {
			Destroy(gameObject);
		}
		else {
			Instance = this;
			DontDestroyOnLoad(gameObject);
			backgroundMusic.mute = (PlayerPrefs.GetInt(PlayerPrefKey.BackgroundMusicOn, 1)) == 0;
			musicToggle.GetComponent<Image>().sprite = backgroundMusic.mute ? noMusicImage : musicImage;
			backgroundMusic.loop = true;
			backgroundMusic.Play();
			soundToggle.GetComponent<Image>().sprite = (PlayerPrefs.GetInt(PlayerPrefKey.SoundOn, 1)) == 0 ? noMusicImage : musicImage;
			int difficultyLevel = PlayerPrefs.GetInt(PlayerPrefKey.DifficultyLevel, 0);
			setDifficultyLevel(difficultyLevel);
		}
	}

	void OnEnable() {
		TapController.OnPlayerDied += OnPlayerDied;
		TapController.OnPlayerScored += OnPlayerScored;
		CountdownText.OnCountdownFinished += OnCountdownFinished;
	}

	void OnDisable() {
		TapController.OnPlayerDied -= OnPlayerDied;
		TapController.OnPlayerScored -= OnPlayerScored;
		CountdownText.OnCountdownFinished -= OnCountdownFinished;
	}

	void OnCountdownFinished() {
		SetPageState(PageState.None);
		OnGameStarted();
		score = 0;
		gameOver = false;
	}

	void OnPlayerScored() {
		score++;
		scoreText.text = score.ToString();
	}

	void OnPlayerDied() {
		void saveIfHigher(string key) {
			int savedScore = PlayerPrefs.GetInt(key, 0);
			if (score > savedScore) {
				PlayerPrefs.SetInt(key, score);
			}
		}
		gameOver = true;
		int difficultyLevel = PlayerPrefs.GetInt(PlayerPrefKey.DifficultyLevel, 0);
		switch (difficultyLevel) {
			case 0:
			saveIfHigher(PlayerPrefKey.HighScoreEasy);
			break;
			case 1:
			saveIfHigher(PlayerPrefKey.HighScoreMedium);
			break;
			case 2:
			saveIfHigher(PlayerPrefKey.HighScoreHard);
			break;
		}

		SetPageState(PageState.GameOver);
	}

	void SetPageState(PageState state) {
		startPage.SetActive(state == PageState.Start || state == PageState.SettingsOpen);
		gameOverPage.SetActive(state == PageState.GameOver);
		countdownPage.SetActive(state == PageState.Countdown);
		settingsPage.SetActive(state == PageState.SettingsOpen);
	}
	
	public void ConfirmGameOver() {
		SetPageState(PageState.Start);
		scoreText.text = "0";
		OnGameOverConfirmed();
	}

	public void StartGame() {
		SetPageState(PageState.Countdown);
	}

    public void ToggleMusic() {
        int musicOn = PlayerPrefs.GetInt(PlayerPrefKey.BackgroundMusicOn, 1);
        PlayerPrefs.SetInt(PlayerPrefKey.BackgroundMusicOn, musicOn == 0 ? 1 : 0);
       	backgroundMusic.mute = PlayerPrefs.GetInt(PlayerPrefKey.BackgroundMusicOn, 1) == 0 ? true : false;
		musicToggle.GetComponent<Image>().sprite = backgroundMusic.mute ? noMusicImage : musicImage;
    }

	public void ToggleSounds() {
        int soundOn = PlayerPrefs.GetInt(PlayerPrefKey.SoundOn, 1);
        PlayerPrefs.SetInt(PlayerPrefKey.SoundOn, soundOn == 0 ? 1 : 0);
		soundToggle.GetComponent<Image>().sprite = soundOn == 1 ? noMusicImage : musicImage;
    }

	public void OpenSettings() {
		SetPageState(PageState.SettingsOpen);
	}

	public void CloseSettings() {
		SetPageState(PageState.Start);
	}

	void setDifficultyLevel(int level) {
		PlayerPrefs.SetInt(PlayerPrefKey.DifficultyLevel, level);
		setDifficultyButtons(level);
	}

	void setDifficultyButtons(int level) {
		easyButton.GetComponent<Image>().sprite = level == 0 ? checkboxSelected : checkboxUnselected;
		mediumButton.GetComponent<Image>().sprite = level == 1 ? checkboxSelected : checkboxUnselected;
		hardButton.GetComponent<Image>().sprite = level == 2 ? checkboxSelected : checkboxUnselected;
	}

	public void SetDifficultyLevelEasy() {
		setDifficultyLevel(0);
	}

	public void SetDifficultyLevelMedium() {
		setDifficultyLevel(1);
	}

	public void SetDifficultyLevelHard() {
		setDifficultyLevel(2);
	}

}
