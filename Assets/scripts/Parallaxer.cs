﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Parallaxer highly inspired by a great tutorial by Renaissance Coders: https://www.youtube.com/watch?v=A-GkNM8M5p8
public class Parallaxer : MonoBehaviour
{

    class PoolObject
    {
        public Transform transform;
        public bool inUse;
        public PoolObject(Transform t) { transform = t; }
        public void Use() { inUse = true; }
        public void Dispose() { inUse = false; }
    }

    [System.Serializable]
    public struct YSpawnRange
    {
        public float minY;
        public float maxY;
    }

    public GameObject Prefab;
    public int poolSize;
    public float shiftSpeed;
    public float spawnRate;

    public YSpawnRange ySpawnRange;
    public Vector3 defaultSpawnPos;
    public bool spawnImmediate;
    public Vector3 immediateSpawnPos;
    public Vector2 targetAspectRatio;

    float spawnTimer;
    float executionTimer;
    PoolObject[] poolObjects;
    float targetAspect;
    GameManager game;

    // DifficultyL Level
    float spawnRangeAddition;
    int speedIncreaseDivider;

    void Awake()
    {
        Configure();
    }

    void Start()
    {
        game = GameManager.Instance;
    }

    void OnEnable()
    {
        GameManager.OnGameOverConfirmed += OnGameOverConfirmed;
		GameManager.OnGameStarted += OnGameStarted;
    }

    void OnDisable()
    {
        GameManager.OnGameOverConfirmed -= OnGameOverConfirmed;
		GameManager.OnGameStarted -= OnGameStarted;
    }

    void OnGameOverConfirmed()
    {
        for (int i = 0; i < poolObjects.Length; i++)
        {
            poolObjects[i].Dispose();
            poolObjects[i].transform.position = Vector3.one * 1000;
        }
        Configure();
    }

    void Update()
    {
        if (game.GameOver) return;

        Shift();
        spawnTimer += Time.deltaTime * (1 + executionTimer / speedIncreaseDivider);
        if (spawnTimer > spawnRate)
        {
            Spawn(false);
            spawnTimer = 0;
        }
        executionTimer += Time.deltaTime;
    }

    void Configure()
    {
        //spawning pool objects
        targetAspect = targetAspectRatio.x / targetAspectRatio.y;
        poolObjects = new PoolObject[poolSize];
        for (int i = 0; i < poolObjects.Length; i++)
        {
            GameObject go = Instantiate(Prefab) as GameObject;
            Transform t = go.transform;
            t.SetParent(transform);
            t.position = Vector3.one * 1000;
            poolObjects[i] = new PoolObject(t);
        }

        if (spawnImmediate)
        {
            Spawn(true);
        }
    }

    void Spawn(bool immediate)
    {
        //moving pool objects into place
        Transform t = GetPoolObject();
        if (t == null) return;
        Vector3 pos = Vector3.zero;
        // Activate the collectible
        if (!immediate)
        {
            if (t.gameObject.tag == "ScoreZone")
            {
                t.gameObject.SetActive(true);
            }
        }
        pos.y = Random.Range(ySpawnRange.minY - spawnRangeAddition, ySpawnRange.maxY + spawnRangeAddition);
        pos.x = ((immediate ? immediateSpawnPos.x : defaultSpawnPos.x) * Camera.main.aspect) / targetAspect;
        t.position = pos;
        if (immediate)
        {
            Spawn(false);
        }
    }

    void Shift()
    {
        //loop through pool objects 
        //moving them
        //discarding them as they go off screen
        for (int i = 0; i < poolObjects.Length; i++)
        {
            poolObjects[i].transform.position += Vector3.right * shiftSpeed * Time.deltaTime * (1 + executionTimer / speedIncreaseDivider); // gradually gets faster
            // Move collectibles up and down
			if (poolObjects[i].transform.gameObject.tag == "ScoreZone")
            {
                float newY = Mathf.Sin((Time.time - i) * ((i + 4) / 4)) * 2;
                Vector3 currentPosition = poolObjects[i].transform.position;
                poolObjects[i].transform.position = new Vector3(currentPosition.x, newY, currentPosition.z);

            }
            CheckDisposeObject(poolObjects[i]);
        }
    }

    void CheckDisposeObject(PoolObject poolObject)
    {
        //place objects off screen
        if (poolObject.transform.position.x < (-defaultSpawnPos.x * Camera.main.aspect) / targetAspect)
        {
            poolObject.Dispose();
            poolObject.transform.position = Vector3.one * 1000;
        }
    }

    Transform GetPoolObject()
    {
        //retrieving first available pool object
        for (int i = 0; i < poolObjects.Length; i++)
        {
            if (!poolObjects[i].inUse)
            {
                poolObjects[i].Use();
                return poolObjects[i].transform;
            }
        }
        return null;
    }

	void OnGameStarted() {
		spawnTimer = 0;
		executionTimer = 0;
        int difficultyLevel = PlayerPrefs.GetInt(PlayerPrefKey.DifficultyLevel, 0);
        switch (difficultyLevel)
        {
            case 0:
                spawnRangeAddition = 0;
                speedIncreaseDivider = 50;
                break;
            case 1:
                spawnRangeAddition = 0.5f;
                speedIncreaseDivider = 25;
                break;
            case 2:
                spawnRangeAddition = 1;
                speedIncreaseDivider = 10;
                break;
        }
	}

}
