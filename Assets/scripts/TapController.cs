﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class TapController : MonoBehaviour {

	public delegate void PlayerDelegate();
	public static event PlayerDelegate OnPlayerDied;
	public static event PlayerDelegate OnPlayerScored;

	public float tapForce = 10;
	public float tiltSmooth = 5;
	public Vector3 startPos;
	public AudioSource tapSound;
	public AudioSource scoreSound;
	public AudioSource dieSound;

	Rigidbody2D rigidBody;
	Quaternion downRotation;
	Quaternion forwardRotation;

	GameManager game;

	void Start() {
		rigidBody = GetComponent<Rigidbody2D>();
		downRotation = Quaternion.Euler(0, 180 , 100);
		forwardRotation = Quaternion.Euler(0, 180, -40);
		game = GameManager.Instance;
		rigidBody.gravityScale = 0;
		rigidBody.simulated = true;
	}

	void OnEnable() {
		GameManager.OnGameStarted += OnGameStarted;
		GameManager.OnGameOverConfirmed += OnGameOverConfirmed;
	}

	void OnDisable() {
		GameManager.OnGameStarted -= OnGameStarted;
		GameManager.OnGameOverConfirmed -= OnGameOverConfirmed;
	}

	void OnGameStarted() {
		rigidBody.velocity = Vector3.zero;
		rigidBody.simulated = true;
		rigidBody.gravityScale = 1;
		bool soundsMuted = PlayerPrefs.GetInt("SoundOn", 1) == 0;
		tapSound.mute = soundsMuted;
		scoreSound.mute = soundsMuted;
		dieSound.mute = soundsMuted;
	}

	void OnGameOverConfirmed() {
		transform.localPosition = startPos;
		transform.rotation = Quaternion.Euler(0, 180 , 0);
	}

	void Update() {
		
		if (game.GameOver) {
			float z = Mathf.Sin(Time.time) * -0.3f;
			transform.Rotate(new Vector3(0, 0, z));
			return;
		}
		
		if (Input.GetKey(KeyCode.RightArrow)) {
			rigidBody.AddForce(Vector2.right * tapForce / 100, ForceMode2D.Force);
		} 

		if (Input.GetKey(KeyCode.LeftArrow)) {
			rigidBody.AddForce(Vector2.left * tapForce / 100, ForceMode2D.Force);
		}

		if (Input.GetMouseButtonDown(0) || Input.GetKeyDown("space")) {
			rigidBody.velocity = Vector2.zero;
			transform.rotation = forwardRotation;
			rigidBody.AddForce(Vector2.up * tapForce, ForceMode2D.Force);
			tapSound.Play();
		}

		transform.rotation = Quaternion.Lerp(transform.rotation, downRotation, tiltSmooth * Time.deltaTime);
	}

	void OnTriggerEnter2D(Collider2D col) {
		if (col.gameObject.tag == "ScoreZone") {
			OnPlayerScored();
			scoreSound.Play();
			col.gameObject.SetActive(false);
		}
		if (col.gameObject.tag == "DeadZone") {
			rigidBody.simulated = false;
			OnPlayerDied();
			dieSound.Play();
		}
	}

}
