﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighscoreText : MonoBehaviour {

	public Text scoreEasy;
	public Text scoreMedium;
	public Text scoreHard;

	void OnEnable() {
		scoreEasy.text = "Easy: " + PlayerPrefs.GetInt(PlayerPrefKey.HighScoreEasy).ToString();
		scoreMedium.text = "Medium: " + PlayerPrefs.GetInt(PlayerPrefKey.HighScoreMedium).ToString();
		scoreHard.text = "Hard: " + PlayerPrefs.GetInt(PlayerPrefKey.HighScoreHard).ToString();
	}
}
