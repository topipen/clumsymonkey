﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static class PlayerPrefKey
{
    public const string BackgroundMusicOn = "BackgroundMusicOn";
    public const string SoundOn = "SoundOn";
    public const string DifficultyLevel = "DifficultyLevel";
    public const string HighScoreEasy = "HighScoreEasy";
    public const string HighScoreMedium = "HighScoreMedium";
    public const string HighScoreHard = "HighScoreHard";
}


